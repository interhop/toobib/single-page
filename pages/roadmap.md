---
layout: roadmap
title: Project
permalink: /roadmap
---

<div markdown="1" class="container roadmap">

# Mission  

Toobib est d'abord un annuaire libre et open-source des professionnels de santé. Il permettra la mise en relation entre les praticiens et les patients dans un environnement libre et open source.  Toobib est une alternative respectueuse de la vie privée des utilisateurs.trices se veut décentralisée et fédérée (Activity Pub).

Ce projet n'utilise que des technologies opensources.

# Objectifs

{:.roadmap-items}
- Respecter la vie privée et des données personnelles des utilisateurs.trices
- Consulter l’annuaire des professionnel.le.s de santé référencés sur Toobib
- Vérifier et corriger les données stockées chez [annuaire.sante.fr](https://annuaire.sante.fr).
- Créer et réserver des plages de rendez-vous
- Fédérer des instances Toobib (utilisation du protocole ActivityPub ?)


# Planning de réalisation

## En 2021

{:.roadmap-items}
- Naissance du projet
- Création d’une [page de présentation de toobib.org](https://toobib.org/).

## En 2022

{:.roadmap-items}
- Faire l'état de l'art des logiciels de prises de rendez-vous opensources existants.
- Commencer à utiliser et adapter un logiciel opensource de prise de rendez-vous
- Partenariat avec une école d'informatique pour aide dans l'amélioration du logiciel opensource de prise de rendez-vous choisi
- Inscription à l'[API Annuaire Santé au format FHIR](https://portal.api.esante.gouv.fr/catalog/api/962f412b-e08e-4ee7-af41-2be08eeee7f6) de l'ANS

## En 2023

{:.roadmap-items}
- Poursuite du développement du logiciel opensouce de prise de rendez-vous
- Déploiement d'un [Cryptpad](https://cryptpad.hds.interhop.org) HDS, et tutoriel pour aide à son utilisation
- Déploiement d'un [BitWarden](https://password.interhop.org), et tutoriel pour aide à son utilisation
- Déploiement d'un [dossier patient associatif et opensource](https://interhop.org/2023/06/06/fabrique-des-santes) HDS, et tutoriel pour aide à son utilisation
- Déploiement d'un gestionnaire d'authentification (Keycloak) HDS
- Déployer une première version du logiciel opensouce de prise de rendez-vous dans nos centres partenaires (version HDS)
- Création d’un formulaire de recherche permettant de questionner la base de données des professionnel.le.s de santé et d’afficher les résultats correspondants sur [Toobib.org](https://toobib.org/)
- Intégration d’un système d'inscription et identification pour les professionnels
- Création d’une page professionnelle permettant d’indiquer en détail les informations du praticien
- Mise en production d'une API FHIR FINESS
- Découverte de l'API Pro Santé Connect
- Discussion avec l'ANS concernant l'utilisation de l'API Annuaire Santé au format FHIR


# En 2024
- Extension des services HDS : visioconférence, messagerie instantanée ...
- Authentification via la e-CPS

# Stack (du site toobib)

Backend :

{:.roadmap-items}
- Base de données : PostgreSQL
- API : Python - Flask

Frontend :

{:.roadmap-items}
- React

# Ressources

{:.roadmap-items}
- [Git Site vitrine](https://framagit.org/interhop/toobib/single-page)
- [Git Toobib](https://framagit.org/toobib)
- [PenPot](https://toobib.org/maquette)
</div>
